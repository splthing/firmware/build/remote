# remote

Binaries of the splthing remote firmware. https://splthing.spacemuffin.com/remote_firmware/ points here.

NOTE this is currently for testing a firmware retrieval system, so these .bin files are **just the blink sketch**, but they have data in the all important `esp_app_desc_t`.

## version history

### 0.0.3
Version 0.0.3 is a real banger.
### 0.0.2
Version 0.0.2 is a real banger.
### 0.0.1
Version 0.0.1 is a real banger.